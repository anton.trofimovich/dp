const path = require("path");
const webpack = require("webpack");
const common = require("./webpack.common");
const merge = require("webpack-merge");

module.exports = merge({
    mode: "development",
    entry: {
        main: [
            "react-hot-loader/patch"
        ]
    },

    devtool: "source-map",

    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    
    optimization: {
        nodeEnv: "development"
    },

    devServer: {
        contentBase: path.resolve(__dirname, "../", "dist"),
        hot: true,
        port: 9070
    }
}, common);
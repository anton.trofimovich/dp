const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: {
        main: [
            path.resolve(__dirname, "../", "src", "index")
        ]
    },

    output: {
        path: path.resolve(__dirname, "../", "dist"),
        publicPath: "/",
        filename: "[name].[hash].js"
    },

    module: {
        rules: [
            {
                test: /.js/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "../", "src", "index.html")
        })
    ],

    resolve: {
        alias: {
            ActionTypes: path.resolve(__dirname, "../", "src", "actions", "types.js"),
            ActionCreators: path.resolve(__dirname, "../", "src", "actions"),
            Components: path.resolve(__dirname, "../", "src", "components"),
            Reducers: path.resolve(__dirname, "../", "src", "reducers"),
            Containers: path.resolve(__dirname, "../", "src", "containers"),
            Constants: path.resolve(__dirname, "../", "src", "constants"),
            Utils: path.resolve(__dirname, "../", "src", "utils")
        }
    },
    
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    name: "vendor",
                    chunks: "initial",
                    enforce: true
                },
                commons: {
                    test: /node_modules/,
                    name: "vendor",
                    chunks: "initial",
                    enforce: true
                }
            }
        }
    }
}
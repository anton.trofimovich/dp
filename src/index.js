import { Provider } from "react-redux";
import ReactDOM from "react-dom";
import React from 'react';

import store from "./store";

import App from "./components/App";

ReactDOM.render(
    <Provider store={store}>
        <App></App>
    </Provider>,
    document.getElementById("root")
);

module.hot.accept();

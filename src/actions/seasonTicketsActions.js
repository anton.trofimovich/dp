import {
    FETCH_SEASON_TICKETS,
    UPDATE_SEASON_TICKET,
    REMOVE_SEASON_TICKET
} from "./types";

let SEASON_TICKETS_MOCK = [
    {
        title: "Family",
        price: 50,
        term: "1 month",
        id: "0"
    },
    {
        title: "Students",
        term: "1 month",
        price: 30,
        id: "1"
    }
]

const makeObj = ({
    from,
    field
}) => {
    return from.reduce((result, nextItem) => {
        result[nextItem[field]] = nextItem;
        return result;
    }, {})
}

const getSeasonTicketsReq = query => {
    return new Promise((resolve, reject) => {
        let _timeout = setTimeout(_ => {
            if (query) {
                let queriedTickets = SEASON_TICKETS_MOCK.filter((ticket, idx) => query(ticket, idx));

                return resolve(makeObj({from: queriedTickets, field: "id"}));
            }

            resolve(makeObj({from: SEASON_TICKETS_MOCK, field: "id"}));
            
            clearTimeout(_timeout);
            _timeout = undefined;
        }, 1000)
    })
}

const findAndUpdateSeasonTicket = (queryCb, updateCb) => {
    return new Promise((resolve, reject) => {
        let _timeout = setTimeout(_ => {
            if (!queryCb) {
                return resolve([]);
            }

            let updated = [];
            SEASON_TICKETS_MOCK = SEASON_TICKETS_MOCK.map((ticket, idx) => {
                if (queryCb(ticket, idx)) {
                    let updatedTicket = updateCb(ticket, idx);
                    updated.push(updatedTicket);

                    return updatedTicket;
                }

                return ticket;
            });

            resolve(updated);
            
            clearTimeout(_timeout);
            _timeout = undefined;
        }, 1000)
    })
}

const findAndRemoveSeasonTicket = queryCb => {
    return new Promise((resolve, reject) => {
        let _timeout = setTimeout(_ => {
            if (!queryCb) {
                return resolve();
            }

            let removed;
            SEASON_TICKETS_MOCK = SEASON_TICKETS_MOCK.filter((ticket, idx) => {
                let mayBeRemoved = queryCb(ticket, idx);
                if (mayBeRemoved) {
                    removed = ticket;
                    return false;
                }

                return true;
            });

            resolve(removed);
            
            clearTimeout(_timeout);
            _timeout = undefined;
        }, 1000)
    })
}

export const getSeasonTickets = query => async dispatch => {
    let tickets = await getSeasonTicketsReq(query);

    dispatch({
        type: FETCH_SEASON_TICKETS,
        payload: { tickets }
    })
}

export const updateSeasonTicket = (queryCb, updateCb) => async dispatch => {
    let result;
    try {
        result = await findAndUpdateSeasonTicket(queryCb, updateCb);
    } catch (err) {
        console.error(err);
    }

    let ticket = result[0];
    dispatch({
        type: UPDATE_SEASON_TICKET,
        payload: { id: ticket.id, ticket }
    })
}

export const removeSeasonTicket = queryCb => async dispatch => {
    let removed;
    try {
        removed = await findAndRemoveSeasonTicket(queryCb);
    } catch (err) {
        console.error(err);
    }

    dispatch({
        type: REMOVE_SEASON_TICKET,
        payload: { id: removed.id }
    })
}
import {
    FETCH_CLIENTS,
    ADD_TODAYS_CLIENT,
} from "./types";
import { findIndex } from "Utils";

let CLIENTS_MOCK = [
    {
        name: "Anton Trofimovich",
        remainedDays: 40,
        id: 0
    },
    {
        name: "Tanya Pavlovskaya",
        remainedDays: 32,
        id: 1
    }
];

const getClientsReq = async props => {
    return new Promise((resolve, reject) => {
        let _timeout = setTimeout(_ => {
            if (props === undefined) {
                return resolve([...CLIENTS_MOCK])
            }

            let { name } = props;
            if (name !== undefined && name !== "") {
                let resClients = CLIENTS_MOCK.filter(client => {
                    return client.name.toLowerCase().slice(0, name.length) === name.toLowerCase() && name
                });

                return resolve(resClients)
            }

            resolve([]);
            
            clearTimeout(_timeout);
            _timeout = undefined;
        }, 1000)
    })
}

const findAndUpdateClientsReq = (query, updateCb) => {
    return new Promise((resolve, reject) => {
        let _timeout = setTimeout(_ => {
            if (!query) {
                return resolve([])
            }

            let updated = [];
            CLIENTS_MOCK = CLIENTS_MOCK.map(client => {
                if (query(client)) {
                    let updatedClient = updateCb(client);
                    updated.push(updatedClient);
                    return updatedClient;
                }

                return client;
            })

            resolve(updated);
            
            clearTimeout(_timeout);
            _timeout = undefined;
        }, 1000)
    })
}

const decerementRemainedDays = async query => {
    let data = await findAndUpdateClientsReq(
        query,
        client => ({ ...client, remainedDays: --client.remainedDays })
    );

    return data[0];
}

export const getClients = ({ name }) => async dispatch => {
    const data = await getClientsReq({ name }) || [];

    if (name) {
        return dispatch({
            type: FETCH_CLIENTS,
            payload: { data }
        })
    }
}

export const addTodaysClient = ({ query, additionalInfo }) => async dispatch => {
    let client = await decerementRemainedDays(query);
    let toAdd = client ? { ...client, ...additionalInfo } : { price: 5, ...additionalInfo };

    dispatch({ 
        type: ADD_TODAYS_CLIENT,
        payload: { data: toAdd }
    })
}
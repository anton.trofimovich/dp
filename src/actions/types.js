
export const FETCH_CLIENTS = "fetch_clients";
export const ADD_TODAYS_CLIENT = "add_todays_client";

export const FETCH_SEASON_TICKETS = "fetch_season_tickets";
export const UPDATE_SEASON_TICKET = "update_season_ticket";
export const REMOVE_SEASON_TICKET = "remove_season_ticket";

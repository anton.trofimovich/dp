export const find = (array, predicate) => {
	const list = Object(array);
	const length = list.length >>> 0;
	let value;

	for (var i = 0; i < length; i++) {
		value = list[i];
		if (predicate(value, i, list)) {
			return value;
		}
	}

	return undefined;
};

export const findIndex = (array, predicate) => {
	const list = Object(array);
	const length = list.length >>> 0;
	let value;

	for (var i = 0; i < length; i++) {
		value = list[i];
		if (predicate(value, i, list)) {
			return i;
		}
	}

	return -1;
};

export const debounce = (cb, ms) => {
	let timer;

	return (...args) => {
		const onComplete = () => {
			cb.apply(this, args);
			timer = undefined;
		}

		if (timer) {
			clearTimeout(timer);
		}

		timer = setTimeout(onComplete, ms);
	}
}
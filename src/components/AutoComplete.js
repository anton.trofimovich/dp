import classNames from "classnames";
import React, { PureComponent } from "react";

import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { MenuItem } from 'material-ui/Menu';
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';

import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';

import { find } from "Utils";

const STYLES = theme => ({
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none'
    },
    suggestionsContainerOpen: {
        position: "absolute",
        zIndex: 1,
        width: "100%",
        marginTop: theme.spacing.unit
    },
    spinner: {
        position: "absolute",
        right: theme.spacing.unit
    }
})
class AutoComplete extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { suggestions: [] };
    }

    componentWillReceiveProps({ suggestions }) {
        this.setState({ suggestions });
    }

    renderInput = inputProps => {
        let { classes, ref, label, isFetching, required, ...other } = inputProps;

        return (
            <TextField
                label={label}
                required
                InputProps={{
                    inputRef: ref,
                    classes: {
                        input: classes.input,
                    },
                    endAdornment: isFetching && (
                        <CircularProgress className={classes.spinner} size={23} color="primary" />
                    ),
                    ...other
                }}
            />
        );
    }

    handleSuggestionsClearRequested = () => {
        this.setState({suggestions: []});
    }

    renderSuggestionsContainer = ({ containerProps, children }) => {
        return (
            <Paper {...containerProps} square>
                {children}
            </Paper>
        );
    }

    getSuggestionValue = suggestion => {
        let { valueField } = this.props;
        return suggestion[valueField];
    }

    renderSuggestion = (suggestion, { query, isHighlighted }) => {
        const matches = match(suggestion.name, query);
        const parts = parse(suggestion.name, matches);
      
        return (
            <MenuItem selected={isHighlighted} component="div">
                <div>
                    {parts.map((part, index) => {
                        return part.highlight ? (
                            <span key={String(index)} style={{ fontWeight: 500 }}>
                                {part.text}
                            </span>
                        ) : (
                            <strong key={String(index)} style={{ fontWeight: 300 }}>
                                {part.text}
                            </strong>
                        );
                    })}
                </div>
            </MenuItem>
        );
    }

    render() {
        let { suggestions } = this.state;
        let { classes, onSuggestionsFetchRequested, isFetching, inputProps } = this.props;

        return (
            <Autosuggest
                theme={{
                    suggestionsList: classes.suggestionsList,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen
                }}
                renderInputComponent={this.renderInput}
                suggestions={suggestions}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
                renderSuggestionsContainer={this.renderSuggestionsContainer}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                inputProps={{
                    classes,
                    isFetching,
                    ...inputProps
                }}
            ></Autosuggest>
        )
    }
}

export default withStyles(STYLES)(AutoComplete);
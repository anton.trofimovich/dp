import React from "react";

import MainAppTabPanel from "Containers/MainAppTabPanel";

class App extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <main className="container-fluid mt-3" id="container">
                <MainAppTabPanel></MainAppTabPanel>
            </main>
        )
    }
}

export default App;
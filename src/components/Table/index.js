import Table from "./Table";
import makeEditable from "./makeEditable";

export default Table;
export {
    makeEditable
}
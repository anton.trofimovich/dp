import React from "react";

import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow
} from "material-ui/Table";

export default ({
    columns,
    data,
    classes,
    renderCell
}) => {
    let renderCells = dataItem => {
        return (
            columns.map((column, idx) => {
                let renderedCell = renderCell && renderCell({ dataItem, column });
                let content = renderedCell || dataItem[column.key] || "-";
                
                return <TableCell key={`${column.key}-data`}>
                    {content}
                </TableCell>
            })
        )
    }

    return (
        <Table>
            <TableHead>
                <TableRow>
                    {columns.map(({ title, key }) => (
                        <TableCell key={`${key}-head`}>{title}</TableCell>
                    ))}
                </TableRow>
            </TableHead>
            <TableBody>
                {data.map((item, idx) =>
                    <TableRow key={idx}>
                        {renderCells(item)}
                    </TableRow>
                )}
            </TableBody>
        </Table>
    )
}
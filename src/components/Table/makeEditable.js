import React, { PureComponent } from "React";

import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';

import isEmpty from "lodash/isEmpty";

const INITIAL_STATE = {
    editingItem: {},
    removingItem: {},
    newValue: {},
    editing: false,
    removing: false
};

const STYLES = theme => ({
    bootstrapInput: {
        borderRadius: 4,
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        fontSize: 14,
        padding: '10px 12px',
        width: 'calc(100% - 12px)',
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
        }
    },
    spinner: {
        color: "white"
    },
    actionButton: {
        marginRight: theme.spacing.unit * 2
    }
})

export default Table => {
class EditableTable extends PureComponent {
        state = INITIAL_STATE;

        reset() {
            this.setState(INITIAL_STATE);
        }

        componentWillReceiveProps(nextProps) {
            if (this.props.data.length > nextProps.data.length) {
                this.setState({ loading: false });
            }

            if (!isEmpty(this.state.editingItem)) {
                this.reset();
            }
        }

        setValue = field => e => {
            this.setState({
                newValue: {
                    ...this.state.newValue,
                    [field]: e.target.value
                }
            })
        }

        insertEditCell = data => (
            data.map(item => (
                { ...item, edit: "edit" }
            ))
        )

        insertEditColumn = columns => (
            [ 
                ...columns,
                {
                    key: "edit",
                    title: ""   
                }
            ]
        )

        toggleEditableState = editingItem => {
            if (isEmpty(editingItem)) {
                this.setState({ editing: true })
                return this.props.onChange({ ...this.state });
            }

            this.setState({ editingItem });
        }

        renderEditableCell = ({ dataItem, column }) => {
            let { classes } = this.props;
            return (
                <TextField
                    value={this.state.newValue[column.key] || dataItem[column.key]}
                    onChange={this.setValue(column.key)}
                    InputProps={{
                        disableUnderline: true,
                        classes: {
                            input: classes.bootstrapInput
                        }
                    }}
                />
            )
        }

        remove = dataItem => {
            this.setState({ removing: true, removingItem: dataItem });
            this.props.onRemove(dataItem);
        }

        renderButtons = dataItem => {
            let { editingItem, removingItem, editing, removing } = this.state;
            let { classes } = this.props;

            const renderEditButtonContent = () => {
                let isEditableState = editingItem.id === dataItem.id;

                if (editing && isEditableState) {
                    return <CircularProgress className={this.props.classes.spinner} size={18} color="secondary" />
                }

                return isEditableState ? "Done" : "Edit";
            }

            const renderDeleteButtonContent = () => {
                let isRemovingState = removingItem.id === dataItem.id;    
                
                if (removing && isRemovingState) {
                    return <CircularProgress className={this.props.classes.spinner} size={18} color="secondary" />
                }

                return "Delete";
            }

            return (
                <React.Fragment>
                    <Button
                        variant="raised"
                        color="primary"
                        size="small"
                        className={classes.actionButton}
                        onClick={() => this.toggleEditableState(!isEmpty(editingItem) ? {} : dataItem)}
                    >
                        {renderEditButtonContent()}
                    </Button>
                    <Button
                        variant="raised"
                        color="primary"
                        size="small"
                        className={classes.actionButton}
                        onClick={() => this.remove(dataItem)}
                    >
                        {renderDeleteButtonContent()}
                    </Button>
                </React.Fragment>
            )
        }

        renderCell = props => {
            if (props.column.key === "edit") {
                return this.renderButtons(props.dataItem);
            }

            if (this.state.editingItem.id === props.dataItem.id) {
                return this.renderEditableCell(props);
            }
        }
    
        render() {
            let props = {
                ...this.props,
                data: this.insertEditCell(this.props.data),
                columns: this.insertEditColumn(this.props.columns)
            }; 

            return (
                <Table
                    className="editable-table"
                    {...props}
                    renderCell={this.renderCell}>
                </Table>
            )
        }
    }

    return withStyles(STYLES)(EditableTable);
}
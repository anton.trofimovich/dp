import { combineReducers } from "redux";
import clientsReducer from "./clientsReducer";
import seasonTicketsReducer, { getTickets } from "./seasonTicketsReducer";

export default combineReducers({
    clients: clientsReducer,
    seasonTickets: seasonTicketsReducer
});

export {
    getTickets
}
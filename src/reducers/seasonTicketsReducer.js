import {
    FETCH_SEASON_TICKETS,
    UPDATE_SEASON_TICKET,
    REMOVE_SEASON_TICKET
} from "ActionTypes";
import { combineReducers } from "redux";

const byIds = (state = {}, action) => {
    switch (action.type) {
        case FETCH_SEASON_TICKETS:
            return {
                ...state,
                ...action.payload.tickets
            };
        case UPDATE_SEASON_TICKET:
            return {
                ...state,
                [action.payload.id]: action.payload.ticket
            };
        case REMOVE_SEASON_TICKET:
            let newState = { ...state };
            delete newState[action.payload.id];
            
            return newState;
        default:
            return state;
    }
}

const allIds = (state = [], action) => {
    switch (action.type) {
        case FETCH_SEASON_TICKETS:
            return Object.keys(action.payload.tickets);
        case REMOVE_SEASON_TICKET:
            return state.filter(id => id !== action.payload.id)
        default:
            return state;
    }
}

export default combineReducers({
    byIds,
    allIds
})

export const getTickets = (state, filter) => {
    if (!filter) {
        return state.allIds.map(id => state.byIds[id]);
    }
}
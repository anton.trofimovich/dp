import {
    FETCH_CLIENTS,
    ADD_TODAYS_CLIENT
} from "ActionTypes";
import { combineReducers } from "redux";

const fetched = (state = [], action) => {
    switch (action.type) {
        case FETCH_CLIENTS:
            return action.payload.data;
        default:
            return state;
    }
}

const todays = (state = [], action) => {
    switch (action.type) {
        case ADD_TODAYS_CLIENT:
            return [ ...state, action.payload.data ];
        default:
            return state;
    }
}

export default combineReducers({
    fetched,
    todays
})
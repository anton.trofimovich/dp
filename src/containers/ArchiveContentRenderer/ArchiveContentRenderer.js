import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getTickets } from "Reducers";

import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Table, { makeEditable } from "Components/Table";
import { CircularProgress } from 'material-ui/Progress';

import { getSeasonTickets, updateSeasonTicket, removeSeasonTicket } from "ActionCreators";

const EditableTable = makeEditable(Table);

const COL_NAMES = [
    {
        key: "title",
        title: "Title"
    },
    {
        key: "price",
        title: "Price"
    },
    {
        key: "term",
        title: "Term"
    }
]

const STYLES = theme => ({
    container: {
        paddingLeft: theme.spacing.unit * 3,
        paddingRight: theme.spacing.unit * 3
    }
})

class ArchiveContentRenderer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false
        };
    }

    componentDidMount() {
        this.setState({isFetching: true})
        this.props.getSeasonTickets();
    }

    componentWillReceiveProps({ seasonTickets }) {
        this.setState({isFetching: false});
    }

    onChange = newData => {
        let { editingItem, newValue } = newData;
        this.props.updateSeasonTicket(
            ticket => ticket.id === editingItem.id,
            ticket => ({ ...ticket, ...newValue })
        )
    }

    onSeasonTicketRemove = dataItem => {
        if (dataItem) {
            this.props.removeSeasonTicket(
                ticket => ticket.id === dataItem.id
            );
        }
    }

    generateTable = () => {
        let { seasonTickets } = this.props;

        if (seasonTickets.length > 0) {
            return (
                <React.Fragment>
                    <EditableTable
                        columns={COL_NAMES}
                        data={seasonTickets}
                        onChange={this.onChange}
                        onRemove={this.onSeasonTicketRemove}
                    ></EditableTable>
                    {this.state.isFetching && <CircularProgress size={30} color="secondary" />}
                </React.Fragment>
            )
        }

        if (this.state.isFetching) {
            return <CircularProgress size={30} color="secondary" />
        }
    }
    
    render() {
        let { classes } = this.props;

        return (
            <Paper>
                <div className={classes.container}>
                    {this.generateTable()}
                </div>
            </Paper>
        )
    }
}

const mapStateToProps = ({ seasonTickets }) => {
    return { seasonTickets: getTickets(seasonTickets) };
}

export default withStyles(STYLES)(connect(
    mapStateToProps,
    { getSeasonTickets, updateSeasonTicket, removeSeasonTicket }
)(ArchiveContentRenderer));
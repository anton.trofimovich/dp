import React, { PureComponent } from "react";
import { connect } from "react-redux";

import tabs from "Constants/tabsData";

import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import RegistrationContentRenderer from "./RegistrationContentRenderer";
import ArchiveContentRenderer from "./ArchiveContentRenderer";

const STYLES = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    content: {
        marginTop: theme.spacing.unit * 2
    }
})

class MainAppTabPanel extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {value: 0};
    }

    handleChange = (e, value) => {
        this.setState({ value })
    }

    render() {
        let { classes } = this.props;
        let { value } = this.state;

        return (
            <div className={classes.root}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        fullWidth
                        centered
                    >
                        {tabs.map(({ title }) => <Tab label={title} key={title} />)}
                    </Tabs>
                </AppBar>
                <div className={classes.content}>
                    {value === 0 && <ArchiveContentRenderer></ArchiveContentRenderer>}
                    {value === 1 && <RegistrationContentRenderer></RegistrationContentRenderer>}
                    {value === 2 && <div>Zhenya</div>}
                    {value === 3 && <div>Lorem Ipsum</div>}
                </div>
            </div>
        )
    }
}

export default withStyles(STYLES)(MainAppTabPanel);
import React from "react";
import { connect } from "react-redux";

import Table from "Components/Table";

const COL_NAMES = [
    {
        key: "name",
        title: "Name"
    },
    {
        key: "price",
        title: "Price"
    },
    {
        key: "key",
        title: "Key"
    },
    {
        key: "remainedDays",
        title: "Remained Days"
    }
]

const VisitorsTable = ({ clients }) => {
    return (
        clients.length > 0 && <Table columns={COL_NAMES} data={clients}></Table>
    )
}

const mapStateToProps = ({ clients }) => {
    return { clients: clients.todays };
}

export default connect(mapStateToProps)(VisitorsTable);
import React, { PureComponent } from "react";
import classNames from "classnames";

import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';

const STYLES = theme => ({
    spinner: {
        color: "white"
    }
})

class VisitorDataForm extends PureComponent {
    renderSubmitButton = ({ theme = {}, isAdding }) => {
        let { classes } = this.props;

        return (
            <Button
                variant="raised"
                className={classNames(theme)}
                color="primary"
                size="small"
                type="submit"
            >
                {isAdding 
                    ? <CircularProgress className={classes.spinner} size={30} color="secondary" />
                    : "Add"
                }
            </Button>
        )
    }

    render() {
        return this.props.render({
            renderSubmitButton: this.renderSubmitButton
        })
    }
}

export default withStyles(STYLES)(VisitorDataForm);
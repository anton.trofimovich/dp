import React, { PureComponent } from "react";
import { connect } from "react-redux";

import AutoComplete from "Components/AutoComplete";
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';

import { getClients, addTodaysClient } from "ActionCreators";
import { debounce } from "Utils";

import formFields from "Constants/seasonVisitorDataFormFields";

const STYLES = theme => ({
    container: {
        display: "flex",
        justifyContent: "center"
    },
    fields: {
        width: 200,
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3
    },
    autoCompleteContainer: {
        position: "relative"
    }
})

class SeasonVisitorDataForm extends PureComponent {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState() {
        return formFields.reduce((prev, next) => {
            prev = {...prev, [next.id]: ""};
            return prev;
        }, {clients: []});
    }

    componentWillReceiveProps({ clients, todaysClients }) {
        if (this.props.clients !== clients) {
            this.setState({ clients, isFetching: false });
        }

        if (todaysClients !== this.props.todaysClients) {
            this.setState({clientName: "", keyNumber: "", isAdding: false});
        }
    }

    getClients = debounce(({ value }) => {
        if (value) {
            this.setState({isFetching: true})
            this.props.getClients({ name: value });
        }
    }, 1000)

    handleNameInputChange = (e, { newValue, method }) => {
        if (newValue === "" || method !== "type") {
            this.setState({ clients: [] })
        } else {
            this.getClients({ name: newValue });
        }

        this.setState({ clientName: newValue });
    }

    handleKeyInputChange = e => {
        this.setState({ keyNumber: e.target.value });
    }

    isClientExist = clientName => {
        return this.props.clients.some(client => client.name === clientName);
    }

    onSubmit = e => {
        e.preventDefault();

        let { clientName, keyNumber } = this.state;
        if (this.isClientExist(clientName)) {
            this.setState({isAdding: true});
            return this.props.addTodaysClient({
                query: client => client.name === clientName,
                additionalInfo: { key: keyNumber }
            });
        }
    }

    renderFields() {
        let { classes } = this.props;
        let { clients, isFetching } = this.state;

        return formFields.map(({ label, id }) => {
            if (id === "clientName") {
                return (
                    <div className={classes.autoCompleteContainer}>
                        <AutoComplete
                            key={id}
                            valueField="name"
                            suggestions={clients}
                            onSuggestionsFetchRequested={this.getClients}
                            isFetching={isFetching}
                            inputProps={{
                                label,
                                id,
                                onChange: this.handleNameInputChange,
                                value: this.state[id],
                                required: true
                            }}
                        />
                    </div>
                )
            }

            return (
                <TextField 
                    id={id}
                    key={id}
                    label={label}
                    className={classes.fields}
                    value={this.state[id]}
                    onChange={this.handleKeyInputChange}
                    required
                />
            )
        })
    }
    
    render() {
        let { classes } = this.props;
        let { isAdding } = this.state;

        return (
            <form className={classes.container} onSubmit={this.onSubmit}>
                {this.renderFields()}
                {this.props.renderSubmitButton({ isAdding })}
            </form>
        )
    }
}

SeasonVisitorDataForm = withStyles(STYLES)(SeasonVisitorDataForm);

const mapStateToProps = ({ clients }) => {
    return {
        clients: clients.fetched,
        todaysClients: clients.todays
    };
}

export default connect(
    mapStateToProps,
    { getClients, addTodaysClient }
)(SeasonVisitorDataForm);
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';

import { addTodaysClient } from "ActionCreators";

const STYLES = theme => ({
    container: {
        display: "flex",
        justifyContent: "center",
        marginTop: theme.spacing.unit * 2
    },
    fields: {
        width: 200,
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3
    }
})

class DailyVisitorDataForm extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {key: ""};
    }

    componentWillReceiveProps({ todaysClients }) {
        if (this.props.todaysClients !== todaysClients) {
            this.setState({key: "", isAdding: false})
        }
    }

    handleKeyInputChange = e => {
        this.setState({key: e.target.value});
    }

    onSubmit = e => {
        e.preventDefault();

        let { key } = this.state;
        if (key && key !== "") {
            this.setState({isAdding: true});
            return this.props.addTodaysClient({
                additionalInfo: { key }
            });
        }
    }

    render() {
        let { classes, renderSubmitButton } = this.props;
        let { key, isAdding } = this.state;

        return (
            <form className={classes.container} onSubmit={this.onSubmit}>
                <TextField 
                    id={"key"}
                    label={"Key"}
                    className={classes.fields}
                    value={key}
                    onChange={this.handleKeyInputChange}
                    required
                />
                {renderSubmitButton({ isAdding })}
            </form>
        )
    }
}

DailyVisitorDataForm = withStyles(STYLES)(DailyVisitorDataForm);

const mapStateToProps = ({ clients }) => {
    return {
        todaysClients: clients.todays
    }
}

export default connect(mapStateToProps, { addTodaysClient })(DailyVisitorDataForm);
import React, { PureComponent } from "react";
import { connect } from "react-redux";

import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Tabs, { Tab } from 'material-ui/Tabs';

import {
    DailyVisitorDataForm,
    SeasonVisitorDataForm,
    VisitorDataForm
} from "./VisitorDataForms";
import VisitorsTable from "./VisitorsTable";

const STYLES = theme => ({
    root: {
        padding: theme.spacing.unit * 3,
        paddingBottom: theme.spacing.unit * 6
    },
    tabsContainer: {
        marginBottom: theme.spacing.unit * 2
    },
    contentsContainer: {
        marginBottom: theme.spacing.unit * 2
    }
})
class RegistrationContentRenderer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {value: 0};
    }

    handleChange = (e, value) => {
        this.setState({ value });
    }

    render() {
        let { value } = this.state;
        let { classes } = this.props;

        return (
            <Paper>
                <div className={classes.root}>
                    <div className={classes.tabsContainer}>
                        <Tabs
                            value={value}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            centered
                        >
                            <Tab label="Season Attend"></Tab>
                            <Tab label="Daily Attend"></Tab>
                        </Tabs>
                    </div>
                    
                    <div className={classes.contentsContainer}>
                        {value === 0 && (
                            <VisitorDataForm
                                render={props => (
                                    <SeasonVisitorDataForm {...props}></SeasonVisitorDataForm>
                                )}
                            ></VisitorDataForm>
                        )}
                        {value === 1 && (
                            <VisitorDataForm
                                render={props => (
                                    <DailyVisitorDataForm {...props}></DailyVisitorDataForm>
                                )}
                            ></VisitorDataForm>
                        )}
                    </div>
                    
                    <VisitorsTable></VisitorsTable>
                </div>
            </Paper>
        )
    }
}

export default withStyles(STYLES)(RegistrationContentRenderer);